<?php

include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->RestartBuffer();
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

use Bitrix\Main\Localization\Loc as Loc;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle('404');
?>
<body>
<!-- Preloader -->
<section id="wrapper" class="container-fluid">
    <div class="error-box">
        <div class="error-body text-center">
            <h1 class="text-danger">404</h1>
            <h3>Page Not Found !</h3>
            <p class="text-muted m-t-30 m-b-30">MOST LIKELY THE PAGE YOU ARE LOOKING FOR THERE</p>
            <a href="/" class="btn btn-danger btn-rounded m-b-40">Back to home</a>

        </div>
    </div>
</section>
</body>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
