$(document).ready(function () {
    $(document).on('submit', '#register-form', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/ajax/register.php',
            cache:false,
            data: $("#register-form").serialize(),
            dataType: 'json',
            success: function (result) {
                if (result.status == 'success') {
                    window.location.replace("http://localhost/");
                    alert(result.message);
                }
                else{
                    if (result['message'] == "phone"){
                        $('#descER').show();
                        $('#descER').delay(500).hide();
                        return;
                    }
                    $('#checkPass').removeAttr("hidden");
                    $('#checkPass').html(result.message);

                }
            }
        });
    });
});
