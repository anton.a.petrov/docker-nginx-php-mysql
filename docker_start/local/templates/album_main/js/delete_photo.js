$(document).ready(function (){

    $(".deletePhoto").click(function (){
        let photo_id = $(this).val();
        let album_id = $('#ALBUM_CODE').val();
        let album_code = $('#ALBUM_NAME').val();
        let creator_id = $('#CREATOR_ID').val();
        let iblock_id = $("#IBLOCK_ID").val();
        let data = {
            photo_id,
            album_id,
            creator_id,
            album_code,
            iblock_id
        };

        $.ajax({
            url: "http://localhost/albums/delete_photo.php",
            type: "POST",
            data: data,
            cache: false,
            dataType: "json",
            success:function (result){
                if(result["status"] ===  'success'){
                    alert("Фотография была успешно удалена");
                    window.location.reload(true);
                }
                else{
                    alert("Во время удаления произошла ошибка");
                }
            }
        })
    })
})