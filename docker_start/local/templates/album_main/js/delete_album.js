$(document).ready(function (){
    $(".deleteBtn").click(function (){
        let data = $(this).val();
        let iblock_id =$("#IBLOCK_ID").val();
        $.ajax({
            url: "deleteAlbum.php",
            type: "POST",
            dataType: 'json',
            data: {data,iblock_id},
            success:function (result){
                if(result["status"] == 'success'){
                    alert("Альбом был успешно удален");
                    window.location.reload();
                }
                else{
                    alert("Во время удаления произошла ошибка");
                }
            }
        })
        return false;
    })
})