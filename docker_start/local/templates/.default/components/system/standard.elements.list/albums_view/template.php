<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?php

use Bitrix\Main\Localization\Loc as Loc;

global $USER;
Loc::loadMessages(__FILE__);
$this->setFrameMode(true);
?>
<input type="text" name="IBLOCK_ID" id="IBLOCK_ID" hidden value="<?= $arResult["IBLOCK_ID"] ?>">
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Основная страница альбомов</h1>
            <p class="lead text-muted"></p>
            <p>
                <a href="add" class="btn btn-primary my-2">Добавить альбом</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <?php foreach ($arResult['ITEMS'] as $item): ?>
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <a href="<?= $item["URL"] ?>">
                                <img class="card-img-top" src='<?= $item["IMG_SRC"] ?>' alt="<? $item["NAME"] ?>"
                                     height="225">
                            </a>
                            <div class="card-body">
                                <p class="card-text"><?= $item["NAME"]; ?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button onclick="document.location='<?= $item["URL"] ?>'"
                                                type="button" class="btn btn-sm btn-outline-secondary">Просмотр
                                        </button>
                                        <button type="button"
                                                class="btn btn-sm btn-outline-secondary deleteBtn"
                                                value="<?= $item["ALBUM_CODE"] ?>">Удалить
                                        </button>
                                    </div>
                                    <small class="text-muted"><? echo $item["CREATED_USER_NAME"]; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?= $arResult['NAV_STRING']; ?>
        </div>
    </div>
</main>