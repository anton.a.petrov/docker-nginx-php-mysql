<?php
require_once __DIR__ . '/vendor/autoload.php';
use Bitrix\Main\Loader;

// Константы
require(__DIR__ . "/include/constants.php");
// Функции
require(__DIR__ . "/include/functions.php");

AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserUpdateHandler");
AddEventHandler("main", "OnBeforeUserUpdate", "OnBeforeUserUpdateHandler");
function OnBeforeUserUpdateHandler(&$arFields)
{
    $arFields["LOGIN"] = $arFields["EMAIL"];
    return $arFields;
}

AddEventHandler("main", "OnBeforeUserLogin", "BeforeUserLoginChangeLogin");
function BeforeUserLoginChangeLogin(&$arFields)
{//проверяем регуляркой e-mail ли
    {
        if (preg_match('/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/', $arFields['LOGIN'])) {
            $dbUsers = CUser::GetList(($by = "id"), ($order = "asc"), array("EMAIL" => $arFields['LOGIN'])); // выбираем пользователя по e-mail
            if ($arUser = $dbUsers->Fetch()) {
                $arFields['LOGIN'] = $arUser['LOGIN']; //меняем e-mail на логин
            }
        }
    }
}

AddEventHandler("main", "OnAfterUserLogin", array("ClassRedirect", "OnAfterUserLoginHandler"));
