<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * variables
 * @var array $arResult
 * @var array $arParams
 * @global $APPLICATION
 */

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
?>
<div class="container">
    <div class="py-5 text-center">
        <img width="72" src="<? $_SERVER['DOCUMENT_ROOT'] ?>/albums/icons/album.svg" height="72"
             class="d-block mx-auto mb-4" alt="">
        <h2>Добавление альбома</h2>
        <p class="lead">
            На данной странице вы можете добавить альбом, указав наименование, описание и добавить фотографию анонса
        </p>
    </div>
    <div class="col-md-8 order-md-1">
        <h4 class="mb-3">Добавление</h4>
        <form action="<?= $APPLICATION->GetCurPage(); ?>" class="needs-validation" novalidate="" id="img-upload">
            <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="ALBUM_NAME">Название альбома</label> <input type="text" class="form-control"
                                                                            id="ALBUM_NAME" placeholder="" value=""
                                                                            required="">
                    <div class="invalid-feedback" id="nameER">
                        Введите название альбома
                    </div>
                </div>
            </div>
            <hr class="mb-4">
            <div class="mb-3">
                <label for="ALBUM_DESCRIPTION">Описание альбома</label> <textarea class="form-control"
                                                                                  id="ALBUM_DESCRIPTION"
                                                                                  placeholder="Введите описание альбома"
                                                                                  required="" rows="5"></textarea>
                <div class="invalid-feedback" id="phoneER">
                    Пожалуйста введите описание альбома
                </div>
            </div>
            <div class="mb-3">
                <label for="img">Выберите картинку для превью</label> <br>
                <input class="fileToUpload" type="file" id="img" placeholder="Имя файла" name="img">
                <div class="invalid-feedback" id="imgER">
                    Пожалуйста выберите изображение для превью
                </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" type="submit" id="upload">Завершить создание</button>
        </form>
    </div>
</div>
<footer class="my-5 pt-5 text-muted text-center text-small">
    <p class="mb-1">
        © 2001-2020 Company Name
    </p>
</footer>
