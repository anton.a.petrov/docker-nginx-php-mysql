<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Информация о видео");
require_once($_SERVER["DOCUMENT_ROOT"] . "/helper/YoutubeHelper.php");
?>
<?
$informer = new \YouTubeHelper\YoutubeHelper();
$video_id = $arResult["VARIABLES"]["VIDEO_CODE"];
$video_info = $informer->getVideoByID($video_id);
if (empty($video_info['items'])) {
    \Bitrix\Iblock\Component\Tools::process404('Page not found',
        true, true, true);
}
var_dump(\GuzzleHttp\json_encode($video_info));
?>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
