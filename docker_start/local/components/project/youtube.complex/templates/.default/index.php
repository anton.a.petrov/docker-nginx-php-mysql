<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("");
?>
<? $APPLICATION->IncludeComponent(
    "project:get.video",
    "",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "COUNT" => "48",
        "PLAYLIST_ID" => "PLHb8DB_PqBc8z1nKTPrbJqpi_hNHq_rye",
        "SHOW_NAV" => "N",
        "SORT_DIRECTION1" => "ASC",
        "SORT_FIELD1" => "ACTIVE_FROM"
    )
); ?>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>