<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * variables
 * @var array $arResult
 * @var array $arParams
 */

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);

?>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Видео YouTube</h1>
            <p class="lead text-muted"></p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <?php foreach ($arResult['ITEMS'] as $item): ?>
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <a href="video/<?= $item["URL"] ?>">
                                <img class="card-img-top" src='<?= $item["IMG_SRC"] ?>' alt="<? $item["TITLE"] ?>"
                                     height="225">
                            </a>
                            <div class="card-body">
                                <p class="card-text"><?= $item["TITLE"]; ?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <a href="video/<?= $item["URL"] ?>"
                                           type="button" class="btn btn-sm btn-outline-secondary">Просмотр
                                        </a>
                                    </div>
                                    <small class="text-muted"><? echo $item["CREATED_USER_NAME"]; ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
