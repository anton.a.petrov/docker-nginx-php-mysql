<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * variables
 *
 * @var array $arResult
 * @var array $arParams
 */
global $USER;

use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__);
?>
<main role="main">
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading"><?= $arResult["ALBUM_INFO"]["NAME"] ?></h1>
            <p class="lead text-muted"><?= $arResult["ALBUM_INFO"]["DESCRIPTION"] ?></p>
            <p>
                <a href="upload"
                   class="btn btn-primary my-2" <? if (!$USER->IsAuthorized()): ?> hidden <? endif; ?>>Загрузить
                    фотографии</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <input type="text" name="ALBUM_CODE" id="ALBUM_NAME" hidden
                   value="<?= $arResult["ALBUM_INFO"]["NAME"] ?>">
            <input type="text" name="ALBUM_CODE" id="ALBUM_CODE" hidden
                   value="<?= $arResult["ALBUM_INFO"]["ID"] ?>">
            <input type="text" name="CREATOR_ID" id="CREATOR_ID" hidden
                   value="<?= $arResult["ALBUM_INFO"]["CREATOR"] ?>">
            <input type="text" name="IBLOCK_ID" id="IBLOCK_ID" hidden
                   value="<?= $arResult["IBLOCK_ID"] ?>">
            <div class="row">
                <?php foreach ($arResult['ITEMS'] as $item): ?>
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top"
                                 src='<?= $item['IMG_SRC'] ?>'
                                 alt="Здесь пока нет картинки"
                                 height="225">
                            <div class="card-body">
                                <p class="card-text"><?= $item['IMG_DSC'] ?></p>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" name="PHOTO_CODE"
                                                class="btn btn-sm btn-outline-secondary deletePhoto"
                                                value="<?= $item["IMG_ID"] ?>" <? if (!$USER->IsAuthorized()): ?> hidden <? endif; ?>>
                                            Удалить
                                        </button>
                                    </div>
                                    <small class="text-muted"><?= $item['DATE'] ?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</main>

