<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION;
$APPLICATION->SetTitle("Загрузить фотографии");

?>
<? $APPLICATION->IncludeComponent(
    "project:photo.upload",
    "",
    [
        "IBLOCK_CODE" => "albums",
        "IBLOCK_TYPE" => "albums",
    ]
); ?>
<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>