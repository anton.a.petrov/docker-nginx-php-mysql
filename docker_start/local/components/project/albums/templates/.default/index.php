<?php
$APPLICATION->IncludeComponent(
    "system:standard.elements.list",
    "albums_view",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "COUNT" => "6",
        "IBLOCK_CODE" => "albums",
        "IBLOCK_TYPE" => "albums",
        "SHOW_NAV" => "Y",
        "SORT_DIRECTION1" => "DESC",
        "SORT_DIRECTION2" => "ASC",
        "SORT_FIELD1" => "ACTIVE_FROM",
        "SORT_FIELD2" => "ID",
        "COMPONENT_TEMPLATE" => "albums_view"
    ),
    false
);