<?php
require($_SERVER["DOCUMENT_ROOT"]
    . "/bitrix/modules/main/include/prolog_before.php");
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
global $USER;
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
) {
    $result = [];
    $recaptcha = new \ReCaptcha\ReCaptcha(SECRET_KEY);

    if (!empty($_POST["g-recaptcha-response"])) {
        $gRecaptchaResponse = $_POST["g-recaptcha-response"];
    } else {
        $result['message']
            = "Ошибка проверки капчи";
        $result['status'] = 'error';
        echo(json_encode($result));
        die;
    }


    $resp = $recaptcha->verify($gRecaptchaResponse);

    if (!$resp->isSuccess()) {
        $result['message']
            = "Пожалуйста пройдите проверку на то, что вы человек";
        $result['status'] = 'error';
        echo(json_encode($result));
        die;
    }
    if (!(empty($_REQUEST['EMAIL'])) and !(empty($_REQUEST['PASSWORD']))
        and !(empty($_REQUEST['CONFIRM_PASSWORD']))
        and !(empty($_REQUEST['PERSONAL_PHONE']))
    ) {
        $login = strip_tags($_REQUEST['EMAIL']);
        $phone = strip_tags($_REQUEST['PERSONAL_PHONE']);
        $password = strip_tags($_REQUEST['PASSWORD']);
        $password_confirm = strip_tags($_REQUEST['CONFIRM_PASSWORD']);
        if (!(preg_match('/^(?!\+$)(\+7[0-9]{10})$/', $phone))) {
            $result['status'] = 'error';
            $result['message'] = "phone";
            header('Content-Type: application/json');
            echo(json_encode($result));
            die();
        }
        $arFields = [
            "EMAIL" => $login,
            "PERSONAL_PHONE" => $phone,
            "LOGIN" => $login,
            "LID" => SITE_ID,
            "ACTIVE" => "Y",
            "GROUP_ID" => [2],
            "PASSWORD" => $password,
            "CONFIRM_PASSWORD" => $password_confirm,
        ];
        $CUser = new CUser;
        $USER_ID = $CUser->Add($arFields);
        $error = html_entity_decode($CUser->LAST_ERROR);
        [$email, $pass, $year] = explode(".", $error);
        if (intval($USER_ID) > 0) {
            $result['status'] = 'success';
            $result['message'] = 'Вы успешно зарегистрировались!';
            $arFields['USER_ID'] = $USER_ID;
            $arEventFields = $arFields;
            $USER->Authorize($USER_ID);
        } else {
            $result['status'] = 'error';
            $result['message'] = $error;
        }
    } else {
        $result['status'] = 'error';
        $result['message'] = 'Все поля обязательны для заполнения';
    }
    header('Content-Type: application/json');
    echo(json_encode($result));
}