<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Главная страница");
?><? $APPLICATION->IncludeComponent(
	"system:standard.elements.list", 
	"main_list", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COUNT" => "6",
		"IBLOCK_CODE" => "albums",
		"IBLOCK_TYPE" => "albums",
		"SHOW_NAV" => "Y",
		"SORT_DIRECTION1" => "DESC",
		"SORT_DIRECTION2" => "ASC",
		"SORT_FIELD1" => "ACTIVE_FROM",
		"SORT_FIELD2" => "ID",
		"COMPONENT_TEMPLATE" => "main_list"
	),
	false
); ?><?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>