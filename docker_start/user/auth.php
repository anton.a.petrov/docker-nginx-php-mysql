<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация");
?>

<?php $APPLICATION->ShowPanel(); ?>

<? $APPLICATION->IncludeComponent("bitrix:system.auth.form", "auth", array(
    "FORGOT_PASSWORD_URL" => "",    // Страница забытого пароля
    "PROFILE_URL" => "/albums/index.php",    // Страница профиля
    "REGISTER_URL" => "/user/registration.php",    // Страница регистрации
    "SHOW_ERRORS" => "Y",    // Показывать ошибки
),
    false
); ?>

<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>